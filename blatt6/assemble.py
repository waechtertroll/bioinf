# Written using python3.3
# python assembly.py -h     for command-line parameters
# Reads data from a FASTA file, creates hashed prefix- and suffix databases. Provides functions to
# find suffixes/prefixes of a given length and to assemble contigs using a greedy algorithm.
# BioPython is not installed on my main system, so some BioPython functions are emulated.

import re
import random
import argparse
import numpy as np

sequences = []     # contains all original sequences
suffixes = {}      # dictionary of suffixes
prefixes = {}      # dictionary of prefixes
ntmap = "ACTG"     # map of nucleotides, needed for hashing
MAX_ELONGATION = 3 # number of nucleotides that may be skipped when elongating greedyly


# reads data from FASTA text file by ignoring non-data lines and stores it into prefix- and suffix databases
# workaround for missing BioPython
def readDataFile( fileName ):
    global args
    sequenceParser = re.compile( "[ACTG]{1,}" )# filter problematic newlines
    inFile = open( fileName, "r" )
    if not args.q: print( "Reading fragments from " + fileName + "..." )
    data = False
    global sequences
    for line in inFile:
        if data:                         # skip non-data lines
            sequences.append( sequenceParser.findall( line )[0] )
        data = not data                  # switch data/non-data mode after every line
    inFile.close()

    # add reverse complements
    sc = sequences.copy()
    for seq in sc: sequences.append( revComplement( seq ) )

        
# reads a reference genome from a FASTA file
# return   reference genome as a string
# workaround for missing BioPython
def readReferenceGenome( fileName ):
    global args
    if not args.q: print( "Reading reference genome from ", fileName, "..." )
    rg = ""
    sequenceParser = re.compile( "[ACTG]{1,}" )
    inFile = open( fileName, "r" )
    for line in inFile: rg += sequenceParser.findall( line )[0]
    return rg


# samples a number of sequences from those in the fasta file and stores them in hash maps
#    n      integer number of sample sequences, omitting sets up all
def getSampleSequences( n=0, reset=False ):
    global args
    if n != 0 and args.v: print( "Sampling ", n, " reads" )
    global sequences; global suffixes; global prefixes
    if n==0: src=set( sequences )          # hash all elements
    else: src=set( random.sample( sequences, n ) )
    if not args.q: print( "Hashing", len( src ), "unique fragments" )
    for seq in src:
        addSequence( seq, suffixes )       # hash sequence in suffix database
        addSequence( seq[::-1], prefixes ) # hash inverted sequence in prefix database
        

# finds the complement to a given samplestring and returns it backwards
# once more because no BioPython here
# input: a    String sequence of ACTG, other chars would lead to strange output strings
# return      reversed complementary string
def revComplement( a ):
    a = a.replace( "A", "t" ).replace( "G", "c" ).replace( "T", "a" ).replace( "C", "g" ).upper()
    return a[::-1]


# calculates a base-4 hash value for a sequence from its leading 10 chars
# input: seq   String sequence of ACTG, other chars would lead to malformed hash values
# return       Integer hash value
def getHash( seq ):
    h = 0                               # hash value
    b = 1                               # base of addition
    for i in range( 9, -1, -1 ):    #...although direction wouldn't matter
        h += ntmap.find( seq[i] ) * b   # add 0-3 * base
        b *= 4                          # increase base
    return h


# stores a key-value pair in the given dictionary
# input: seq       string sequence
#        database  dictionary in which to store (optional, default is suffix-database)
def addSequence( seq, database=suffixes ):
    key = getHash( seq )
    if not key in database: database[key] = [] # multiple entries per hash possible,
    database[key].append( seq )         # filtering done when elongating
    

# finds a suffix of given length
#  seq   sequence that requires a suffix
#  n     length of desired suffix
#  db    database to search( suffix or prefix )
def findSuffix( seq, n, db=suffixes ):
    key = getHash( seq[n:] )          # hash substring of seq shortened by n
    val = db.pop( key, None )
    if val == None:
        return None
    else:
        found = ""
        for suf in val:
            if suf.startswith( seq[n:] ): # compare prefixes
                if found != "": return None # ambigous suffix, cancelling
                else: found = suf     # matching suffix found
        # remove used reads from suffix/prefix database
        if db == suffixes: removePrefix( suf, prefixes )
        else: removePrefix( suf,  suffixes )
        return found[-n:]             # return suffix


# removes a prefix from the database, to be called after it has been used as a suffix, or the other way around
# (seriously slows down processing)
# input: p    prefix as string
#        db   from which database should be removed?
def removePrefix( p,  db=prefixes ):
    p = p[::-1]
    key = getHash( p )
    val = db.pop( key, None )
    if val != None and len( val ) > 1:
        db[key] = []
        for el in val[1:]:
            db[key].append( el )
    pass


# elongate a given contig to the right
# input: contig    string to be elongated
#        l         length of reading frame
#        database  database to use (prefixes or suffixes)
# returns: contig as string
def elongate( contig, l, database ):
    found = True
    while found:                        # loop: add suffixes until ambiguities are encountered
        for j in range( 0, MAX_ELONGATION ): # try elongating by 1..MAX_ELONGATION nts
            ext = findSuffix( contig[-l:], j+1, database )
            if ext != None: break
        if ext != None: contig += ext
        else: found = False             # if no disambigous match could be found, end elongating
    return contig
    
            

# creates a contig from a random seed
# extends the contig forward in steps from 1 to max_extend until no elongation possible
# then reverses the contig and repeats the same procedure with reversed suffixes = prefixes
# returns the contig as string
def createContig():
    global args
    contig_seed = suffixes.pop( random.choice( list( suffixes.keys() ) ) ) # start with random sequence
    if args.v: print( "Starting contig_seed from: ", contig_seed[0] )
    if len( contig_seed ) > 1:       # check if more than one element was in the popped hash
        for seq in contig_seed[1:]:  # push back unused sequences
            addSequence( seq )
    contig = contig_seed[0]          # extract first sequence from the random hash
    l = len( contig )                # set length of read frame
    contig = elongate( contig, l, suffixes )  # elongate to the right 
    contig = elongate( contig[::-1], l, prefixes ) # elongate to the left by elongating inverse to the right
    if len( contig ) > l and ( not args.q or args.v ):
        print( "--> contig of length ", len( contig ), " could be created" )
    return contig[::-1]          # invert contig back and return it
    #else: return None


# tries to match contigs to a reference genome and find Nx numbers
# input:   contigs   list of contig-strings
#          coverage  float fraction [0-1] of genome to be covered
# return   integer Nx number, because it was requested...
def genomeStatistics( contigs, coverage ):
    global args
    global reference; l = len( reference )
    cover = np.zeros( l, dtype=float )
    n = l                            # length of smallest used contig
    for c in contigs:                # process list of length-sorted contigs
        n = len( c )
        x = reference.find( c )      # x = starting position of contig c inside reference
        if x <= 0: x = reference.find( revComplement( c ) ) # try inverse
        if x > 0:
            cover[x:x+n] = 1.0       # set covered positions to 1.0
            if args.v: print( str( int(sum(cover)/l*100 ) )+"%: contig of length ", n, " matches pos ", x )
        else: print( "Error - one contig could not be matched!" )
        if sum( cover )/l >= coverage: break
    if sum( cover )/l >= coverage: print( "N"+str( int( coverage*100 ) ), "=", n )
    else: print( "Could not cover", str( int( coverage*100) )+"%" )
    return n


# MAIN

random.seed()
# process command line arguments
parser = argparse.ArgumentParser()
parser.add_argument( "-f", help="FASTA file of read fragments", default="smallReads.fna", dest="fileName" )
parser.add_argument( "-c", help="FASTA file with reference DNA", default="NC_001669.fna", dest="referenceFileName" )
parser.add_argument( "-n", help="number of reads to sample, default: use all reads", default=0, dest="nSamples", type=int )
parser.add_argument( "-v", help="verbose mode", action="store_true" )
parser.add_argument( "-q", help="silent mode", action="store_true" )
args = parser.parse_args()
# prepare assembly
readDataFile( args.fileName )
reference = readReferenceGenome( args.referenceFileName )
getSampleSequences( args.nSamples )
# create contigs until all read fragments used
contigs = []
while len( suffixes ) > 0:
    contigs.append( createContig() )
contigs = sorted( [x for x in contigs if x != None], key=len, reverse=True )
print( "\nSampled", args.nSamples,"reads from", args.fileName+":" )
print( len( contigs ), " contigs found" )
genomeStatistics( contigs, 0.5 )
genomeStatistics( contigs, 0.7 )
genomeStatistics( contigs, 0.9 )
