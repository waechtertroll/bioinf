# generate a sequence of nucleotides using Markov chains
# note that matrices use cumulative propabilities for simplified sampling

import random

nts = 'ACGT'

fair = 0
cpg = 1

# transition matrix
hmm_trn = [[0.98, 1.0 ],
         [0.03, 1.0] ]

hmm_probs = {
    fair: [0.25, 0.5, 0.75, 1.0],
    cpg:  [0.1, 0.5, 0.9, 1.0]
}

def next_nt( state, trn, prob ):
    if random.random() <= trn[state][fair]: state = fair      # select next state
    else: state = cpg
    nt = 0
    rnum = random.random()
    while rnum > prob[state][nt]: nt += 1            # select index of nucleotide
    return ( nts[nt], state )

def genSeq( n=1000 ):
    state = fair                            # missing data, assumed to be fair :D
    seq = ''
    modes = []                      # could also use string if output required...
    for i in range( 0, n ):
        smpl = next_nt( state, hmm_trn, hmm_probs )             # get next sample
        state = smpl[1]                           # change state to sampled state
        seq += smpl[0]
        modes.append( state )
    return ( seq, modes )

def firstIslandLength( data ): # do very basic RLE
    foundIsland = False
    islandLength = 0
    lengths = []                                        # array of island lengths
    for state in data:
        if state == fair:
            if foundIsland:                                        # island ended
                lengths.append( islandLength )
                islandLength = 0
                foundIsland = False
        else:
            foundIsland = True
            islandLength += 1
    return lengths

nseqs = 1000
lengths = []
nums = []
print( "Generating", nseqs,"sequences..." )
for i in range( 0, nseqs ):
    stats = firstIslandLength( genSeq()[1] )        # create sequence to get data
    lengths.append( stats[0] )                     # save lengths of first island
    nums.append( len( stats ) )              # save number of islands in this run
    
print( "Average island length:", sum( lengths )*1.0 / nseqs )
print( "Average number of islands:", sum( nums )*1.0 / nseqs )