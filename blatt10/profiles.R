nts <- c( 'A', 'C', 'G', 'T' )

makeSequence <- function( l=2000, p=c(1,1,1,1) ) {
  tmpchars <- sample( nts, l, p, replace=TRUE )
  str <- paste( tmpchars, collapse="" )
  return( str )
}

propability <- function( code, size=20000, num=1000, p=c(1,1,1,1) ) {
    mydata <- array( dim=c(num, size) )
    for( i in 1:num ) {
        mydata[i] <- makeSequence( l=size, p=p )
    }
    return( sum( grepl( code, mydata) ) / num )
}

results <- c()
for( i in seq( 1000, 20000, length=20 ) ) {
    oneresult = propability( "GATACA", size=i )
    results <- c( results, oneresult )
    print( append( "", c("size=", i, " p=", oneresult) ) )
}

plot( results, type="l", ylab="chance for GATACA", xlab="steps between 1000 and 20000" )
abline( h=0.95, col="blue" )
