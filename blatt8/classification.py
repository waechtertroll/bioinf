import numpy as np
import re
import argparse
import matplotlib.pyplot as plt

nts = 'ACGT'


# read a FASTA file manually
# fileName    string containing the filename
# silent      boolean to requlate verbosity
# returns     list of sequences
def readFASTA( fileName, silent=False ):
    if not silent: print( 'reading', fileName, 'as FASTA' )
    filterSeq = re.compile( '[ACGT]{1,}' )
    fileHandle = open( fileName, 'r' )
    seqs = []
    seq = ''
    for line in fileHandle:
        result = filterSeq.findall( line )
        if len( result ) > 0:
            seq += result[0]
        else:
            if len( seq ) > 0: seqs.append( seq )
            seq = ''
    if not silent: print( '--> found', len( seqs), 'sequences' )
    return seqs

# get the codon corresponding to a codon code number
# n           integer range 0-63
# returns     string codon-triplet
def num2codon( n ):
    global nts
    codon = ''
    codon = nts[n%4]+codon; n = int( n/4 )
    codon = nts[n%4]+codon; n = int( n/4 )
    codon = nts[n]+codon
    return codon

# encode a codon-triplet to an integer number for array indexing
# codon       string codon triplet
# returns     integer
def codon2num( codon ):
    return nts.find(codon[0])*16+nts.find(codon[1])*4+nts.find(codon[2])

# count codon frequencies
# db          list of sequences
# returns     array of frequencies
def codonFrequencies( db, silent=False ):
    global nts
    frqs = np.zeros( (len(db),64), dtype=float )
    row = 0
    for seq in db:
        fraction = 3.0/len(seq)             # saves normalising afterwards
        # select correct number of triplets to test
        if len(seq)%3 == 0: rng = range( 0, len(seq), 3 )
        else: rng = range( 0, len(seq)-1, 3 )
        # calculate array index for triplets and add relative frequencies
        for i in rng:
            frqs[row][codon2num(seq[i:i+3])] += fraction
        row += 1
    return frqs

# predicts if a sequence is closer to coding region or utr
# seq       array of frequencies of the sequence to predict
# cx        integer code for codon1
# cy        integer code for codon2
# c_utr     (float,float) tuple centromer for utr
# c_cdr     (float,float) tuple centromer for coding region
def predict( seq, cx, cy, c_utr, c_cdr ):
    dist_utr = (seq[cx]-c_utr[0])**2 + (seq[cy]-c_utr[0])**2
    dist_cdr = (seq[cx]-c_cdr[0])**2 + (seq[cy]-c_cdr[0])**2
    if dist_utr < dist_cdr: return 0
    else: return 1

# print out a codon frequency table
# t           numpy array with 64 columns
# cols        integer number of cols to print
# title       string alternative title to display
# total       boolean should the total be calculated
def printTable( t, cols=4, title='Relative frequencies of nucleotides in percent:', total=True ):
    print( '\n'+title )
    output = ''
    stats = np.sum( t, axis=0 )/t.shape[0]
    for i in range( 0, 64 ):
        output += num2codon(i) + ': ' + str(stats[i]*100)
        if (i+1)%cols == 0:
            print( output )
            output = ''
        else: output += '\t'
    if total: print( 'Total:', np.sum(stats)*100, '%\n' )
    else: print( '\n' )


# MAIN --------------------------------------------------------------------------

# parse command line parameters
parser = argparse.ArgumentParser()
parser.add_argument( '-u', help='training file of UTRs', required=True, dest='utrFile' )
parser.add_argument( '-c', help='training file of coding regions', required=True, dest='cdrFile' )
parser.add_argument( '-t', help='file to test after training', dest='testFile' )
parser.add_argument( '-x', help='x-axis codon for centromer', dest='cx', default='AAA' )
parser.add_argument( '-y', help='y-axis codon for centromer', dest='cy', default='AAC' )
args = parser.parse_args()

# read training files
utr = readFASTA( args.utrFile )
coding = readFASTA( args.cdrFile )
print( 'Calculating codon frequencies in training files...')
f_utr = codonFrequencies( utr )
f_coding = codonFrequencies( coding )

# if no testfile - print tables
if args.testFile == None:
    printTable( f_utr, title='Codon frequencies in UTRs (%)' )
    printTable( f_coding, title='Codon frequencies in coding sequences (%)' )
    printTable( f_coding-f_utr, title='differences (coding - utr)', total=False )

# if testfiles, try to predict coding regions
else:
    print( 'Calculation codon frequencies in test file...' )
    test = readFASTA( args.testFile )
    f_test = codonFrequencies( test )
    mf_utr = np.sum( f_utr, axis=0 ) / f_utr.shape[0] # calculate mean frequencies
    mf_cdr = np.sum( f_coding, axis=0 ) / f_coding.shape[0] # sum cols, divide by rownum
    ncx = codon2num( args.cx.upper() )
    ncy = codon2num( args.cy.upper() )
    mylist = []
    i = 1
    for seq in f_test:
        p = predict( seq, ncx, ncy, (mf_utr[ncx],mf_utr[ncy]), (mf_cdr[ncx],mf_cdr[ncy]) )
        if p == 0: print( 'Sequence', i, 'is propably not coding' )
        else: print( 'Sequence', i, 'is propably coding' )
        i += 1
        mylist.append(p)

    plt.plot( range(0, len(test)), mylist)
    plt.title( (args.cx+' ~ '+args.cy).upper() )
    plt.show()