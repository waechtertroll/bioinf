# Assignment 1 - work with ALL.csv
#
# seperator = ,
# File infos:
# size ~ 26M
# $ wc -l ALL.csv 
# >  12626
#   -> 12625 rows/genes
# $ sed -n '1p' ALL.csv | sed 's/\,/ /g' | wc -w 
# >  128
#   -> 128 cols/samples because first heading is a blank

import numpy as np

#read csv file
theFile = open('ALL.csv', 'r')
sampleNames = theFile.readline().split( ',' )[1:] # first lines contains colnames
geneNames = []
expVals = []
for line in theFile.readlines(): # process rest of file
    vals = line.split( ',' )     # split by comma
    geneNames.append( vals[0] )  # first value is row name
    expVals.append( list( map( float, vals[1:] ) ) ) # rest is values

# sort gene name- and value lists by highest mean of value list
geneNames, expVals = zip( *sorted( zip(geneNames,expVals), key=lambda x: sum(x[1])/len(expVals[0]), reverse=True ) )

# make array out of 100 values with highest means
myArray = np.array( expVals[0:99] )

# display name and mean of 100th highest gene
print( 'Gene with weakest mean of 100 strongest: ' + geneNames[99] + ', value=' + str(sum(expVals[99])/len(expVals[0])) )

# $ python matrixtask.py
# > Gene with weakest mean of 100 strongest: 32588_s_at, value=11.144564190302866