X11()
for ( N in 1:500 ) {
  values <- rexp( N*1e3 )
  y <- c()
  for ( i in 1:1e3 ) y[i] <- mean( values[i*N:i*N+N] )
  hist( y )
  title( sub=paste( "N=", N, ", mean = ", mean( values ), sep="" ) )
}
