# Reads data from a FASTA file, processes it and takes samples

import random
import numpy as np
import matplotlib.pyplot as plt

origReads = []                           # unmodified dataset
uniqueReads = set()                      # dataset of unique fragments

# reads data from FASTA text file by ignoring non-data lines
# encapsulated for future uses
def getData( fileName ):                 # Biopython not installed here :-)
    inFile = open( fileName, "r" )
    isData = False
    print( "Reading file " + fileName + "..." )
    for line in inFile:
        if ( isData ):                   # only every second line contains data
            origReads.append( line )
        isData = not isData
    inFile.close()

# finds the complement to a given samplestring and returns it backwards
def revComplement( a ):
    a = a.replace( "A", "t" ).replace( "G", "c" ).replace( "T", "a" ).replace( "C", "g" ).upper()
    return a[::-1]

# main
getData( "smallReads.fna" )
uniqueReads = set( origReads )                # remove duplicates by converting to set of uniques
print( "Uniques: " + str( len( uniqueReads ) * 100.0 / len( origReads ) ) + "% of " + str( len( origReads ) ) )

print( "Adding reverse complements..." )
uniquesCopy = list( uniqueReads )             # copy unique fragments to non-unique structure
for el in uniqueReads:
    uniquesCopy.append( revComplement( el ) ) # add reverse complements to nun-unique structure
print( "Uniques: " + str( len( set( uniquesCopy ) ) * 100.0 / len( uniquesCopy ) ) + "% of " + str( len( uniquesCopy ) ))

# take samples
random.seed()
nsamples = [2500, 5000, 10000, 20000, 40000, 80000]                     # define sample sizes
ysamples = list( map( lambda x: len( set( random.sample( origReads, x ) ) ), nsamples ) )
print( nsamples )
print( ysamples )

plt.plot( nsamples, ysamples )
plt.show()
