N.ROW <- 10000
N.COL <- 500

vals.max <- vector( mode="numeric", length=N.ROW )
cat( "Using", N.COL, "samples\n" )
if (N.COL < 1000 ) { # fast
  cat( "-> writing whole matrix to ram\n" )
  vals <- matrix( rnorm( N.ROW*N.COL ), nrow=N.ROW, ncol=N.COL )
  vals.max <- apply( vals, 2, max )
} else { # memory-efficient
  cat( "-> filling maximum vector directly\n" )
  for (i in 1:N.ROW) vals.max[i] = max( rnorm( N.COL ) )
}

gumbel <- function( x, mu=0, beta=1 ) {
  exp( -exp( -(x-mu)/beta ) )
}

par( mfrow=c( 2, 2 ) )

hist( vals.max, breaks=50 ); mtext( mean(vals.max), 3 ); abline( v=mean(vals.max), col="blue" )
plot( density( vals.max ) )
qqnorm( vals.max )
qqplot( gumbel( vals.max, mean(vals.max) ), vals.max )
title( "Q-Q extreme value distribution" )
