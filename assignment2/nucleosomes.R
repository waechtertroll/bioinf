# initialize parameters
G <- 40              # length of genome
T <- 5000            # time limit

# initialize nucleosomes randomly
N <- 5                               # number of nucleosomes
NP <- matrix(0,nrow=T,ncol=N)        # NP[t,i] Position of Nucleosome i at time t
NP[1,] <- sort(sample(1:G,N))        # Randomly initialize Positions at t=1
D <- sample(c(-1,1),N,replace=TRUE)  # direction per nucleosome

# constants for plotting
xrange <- c( 0, G+1 )
yrange <- c( -1, 1 )
plotall <- FALSE   # should every step be plotted?

# initialize two nucleosomes
# copied from nucleosome_test.R, uncomment to apply
#NP[1,] <- 2*1:10                 # Position to meet joined  when we change direction
#D      <- rep(-1, 10)            # direction per nucleosome
#T <- 500                         # optionally decrease test time while plotting
#plotall <- TRUE                  # slooooow!

# occupied positions counter
occupied <- rep( 0, G )

# plotting function
plot.at.time <- function( t ) {
  plot.new()
  plot.window( xrange, yrange )
  for (n in 1:N) {
    points(NP[t,n], y=0, pch=n) # plot each nucleosome with different symbol at its position on the genome
  }
  title( paste( "t =", t ) )
}

# main function
for (t in 2:T) {
  # check directions of all nucleosomes
  tmp <- NP[t-1,]+D               # theoretical positions if all nucleosomes would keep their directions
  colissions <- which( tmp[1:G-1] >= tmp[2:G] ) # elements colliding with their right neighbours need to bounce
  colissions <- unique( c( colissions, colissions+1 ) ) # their right neighbours need to bounce, too; unique() should not strictly be neccessary
  D[colissions] <- -D[colissions] # change directions of bouncing nucleosomes
  D[tmp<1] <- 1                   # don't fall from the start of the genome
  D[tmp>G] <- -1                  # don't fall from the end of the genome
  # apply corrected directions
  NP[t,] <- NP[t-1,]+D
  # update counter
  occupied[NP[t,]] <- occupied[NP[t,]] + 1 # increase elements at each nucleosome's position
  if (plotall) plot.at.time( t )  # really slow
}

barplot( occupied )
