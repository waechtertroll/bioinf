from mybio import readFASTA
from mybio import rle
import numpy as np
import array

class HMM: # define Hidden Markov model class to group matrices and values
    def __init__( self ):
        self.states = [0,1]
        self.statenames = { 0: 'FAIR',
                            1: 'CPG' }
        self.symbols = { 'A': 0,
                         'C': 1,
                         'G': 2,
                         'T': 3 }
        self.trn = [[.98, .02],[.03,.97]]
        self.trn = np.log( self.trn )
        self.emm = { 0: np.log([.25, .25, .25, .25 ]),
                     1:  np.log([.1, .4, .4, .1]) }        

def viterbipath( m, seq ): # use classic Viterbi-algorithm
    V = np.zeros( (2, len(seq)), dtype=float )
    T = np.zeros( (2,len(seq)), dtype=int )
    pi = np.zeros( len(seq), dtype=int )
    for i in m.states:
        V[i][0] = 1.0
    # calculate Viterby matrix and store paths
    for i in range( 1, len( seq ) ):
        for t in m.states:
            V[t,i] = m.emm[t][ m.symbols[ seq[i] ] ] + max( V[:,i-1] + m.trn[:,t] )
            T[t,i] = np.argmax( V[:,i-1] + m.trn[:,t] )
    # backtrace
    pi[len(seq)-1] = np.argmax( V[:,len(seq)-1] )
    for i in range( len(seq)-1, 1, -1 ):
        pi[i-1] = T[pi[i],i]
    return pi

seq = readFASTA( 'longSequence.fna', statistics=True )[0]
model = HMM()

vpath = viterbipath( model, seq )
# convert
string = ''
for c in vpath: string += str( c )

print( "Viterbi path:\n", string )
print( rle( vpath ) )
