# Module containing frequently used functions


# read a FASTA file manually
# fileName    string containing the filename
# silent      boolean to requlate verbosity
# returns     list of sequences
def readFASTA( fileName, silent=False, statistics=False ):
    import re
    if not silent: print( 'reading', fileName, 'as FASTA' )
    filterSeq = re.compile( '[ACGT]{1,}' )
    fileHandle = open( fileName, 'r' )
    seqs = []
    seq = ''
    for line in fileHandle:
        result = filterSeq.findall( line )
        if len( result ) > 0:
            seq += result[0]
        else:
            if len( seq ) > 0: seqs.append( seq )
            seq = ''
    if not silent: print( '--> found', len( seqs), 'sequences' )
    if statistics and not silent:
        sum_len = sum([len(seq) for seq in seqs])
        print( "--> average length:", sum_len*1.0/len(seqs) )
    if not silent: print()
    return seqs

# get the codon corresponding to a codon code number
# n           integer range 0-63
# returns     string codon-triplet
def num2codon( n ):
    global nts
    codon = ''
    codon = nts[n%4]+codon; n = int( n/4 )
    codon = nts[n%4]+codon; n = int( n/4 )
    codon = nts[n]+codon
    return codon

# encode a codon-triplet to an integer number for array indexing
# codon       string codon triplet
# returns     integer
def codon2num( codon ):
    return nts.find(codon[0])*16+nts.find(codon[1])*4+nts.find(codon[2])

# run-length encode a bit-patter
# pattern     list of zeros and ones
# returns     list of tuples (0/1, count)
def rle( pattern ): # do run-length encoding to measure CPG-islands
    code = []
    count = 0
    state = pattern[0]
    for el in pattern:
        if el != state:
            code.append( (state, count) )
            count = 1
            state = el
        else:
            count += 1
    code.append( (state,count) ) # last tuple is not followed by state switch
    return code
