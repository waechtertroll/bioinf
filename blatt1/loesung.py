#!/usr/bin/env python32
# -*- coding: utf-8 -*-

# to run this skript extract authorClassification.zip in studentData

# imports
from glob import glob
from collections import Counter, defaultdict
import string, os, numpy as np
import matplotlib.pyplot as plt

# read and count letters in books and text - function
alphabet = string.ascii_lowercase + "äöüß"

def readBook(filename, chars = alphabet):
    f = open(filename, "rb")
    text = f.read().decode("utf-8")
    f.close()

    text = text.lower()
    cnt = Counter(text)
    ret = np.array(list(map(lambda x: cnt[x], chars)), dtype=float)
    rel = np.array(list(map(lambda x: cnt[x], alphabet)), dtype=float)
    ret /= sum(rel)
    return ret


# determine centroids for prediction - function
def computeCentroids(relFrequencies):
    centroids = {}
    centroids["characters"] = relFrequencies["characters"]
    for key in relFrequencies:
        if key != "characters":
            relFrequencies[key] = np.array(relFrequencies[key])
            centroids[key] = relFrequencies[key].sum(0) / relFrequencies[key].shape[0]
    return centroids

# generate image
def plotAuthors(centroids, traindata, letters):
    c1 = centroids["characters"].index(letters[0])
    c2 = centroids["characters"].index(letters[1])
    for key in traindata:
        if key != "characters":
            pdat = traindata[key].T
            plt.plot(pdat[c1], pdat[c2], 'x', centroids[key][c1], centroids[key][c2], "o")
    plt.show()


# read training data and count relative frequencies per letter - in main program
traindata = defaultdict(list)
traindata["characters"] = alphabet
os.chdir("studentData")
for fn in glob("[FH]*/*.txt"):
    ds = readBook(fn)
    key = os.path.dirname(fn)
    traindata[key].append(ds)

# find centroids for all letters und choose the best - in main program
centroids = computeCentroids(traindata)
differences = centroids["Friedrich_Nietzsche"] - centroids["Heinrich_von_Kleist"]
for i in range(len(alphabet)):
    if abs(differences[i]) > 0:
        print("Difference: ", alphabet[i], "\t", differences[i])

# plot for some strong and some weak classifiers
plotAuthors(centroids, traindata, "es")
plotAuthors(centroids, traindata, "nr")
plotAuthors(centroids, traindata, "bf")   # weak different mean, but predicts well

