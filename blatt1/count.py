#!/usr/bin/python
#-*- coding: utf-8 -*-

# Predicting authors of text from training data by comparing relative letter
# frequencies or word and sentence lengths
#
# For usage start without arguments

import sys
import numpy as np
from collections import Counter
import matplotlib.pyplot as pplot
import re

strAlph = "abcdefghijklmnopqrstuvwxyzäöüß" # Alphabet to use

# Function to read frequencies of chars from a file.
# Counted chars are defined by global string "strAlph"
#   fileName: string of system file name to be opened
#   returns array of frequencies
def countChars( fileName ):
    inFile = open( fileName, "rb" )    
    strBuf = str( inFile.read().decode( "utf-8" ).lower() )
    inFile.close()
    cnt = Counter( strBuf )    # count all chars and map relevant chars to an array
    tmp = np.array( list( map( lambda x: cnt[x], "abcdefghijklmnopqrstuvwxyzäöüß" ) ), dtype = float )
    dCount = np.array( list( map( lambda x: cnt[x], strAlph ) ), dtype = float )
    totalChars = sum( dCount )
    dCount /= sum( tmp )
    return dCount

#function to read lengths of words and sentences
#   fileName: string of system file name to be opened
#   returns array [ avg word len, avg sentence len ]
def countWords( fileName ):
    dCount = [0.0, 0.0]
    inFile = open( fileName, "rb" )
    strBuf = inFile.read().decode( "utf-8" ).lower()
    inFile.close()
    mywords = re.compile( "[\w]{1,}[\.\?!]?" )  # compile re's for speed
    mysentences = re.compile( "[\.\?!]" )
    wordlist = mywords.findall( strBuf )        # differs by 1% from % wc -w 
    sentences = mysentences.findall( strBuf )
    for el in wordlist:
        dCount[0] += len( el )
    dCount[0] /= 1.0 * len( wordlist )        # num of chars divided by num of words
    dCount[1] = 1.0 * len( wordlist ) / len( sentences ) # num words divided by num sentences
    return dCount
    

#function to calculate a centroid from a list of dictionaries of relative frequencies
#   data: array of frequencies of current author
#   returns 2-dimensional array (centroid)
def calcCentroid( data ):    # assumes alphabet has at least two letters
    c = [0.0, 0.0]           # centroid
    for element in data:     # sum up all frequencies of each text for both chars
        c[0] += element[0]
        c[1] += element[1]
    c[0] /= len( data )
    c[1] /= len( data )
    return c

#function to match a text to an author using centroids
#   text: array of frequencies
#   ca:   centroid for first author
#   cb:   centroid for second author
# assumes text, ca, cb have each at least 2 elements
def authorMatch( text, ca, cb ):
    distA = (text[0]-ca[0])**2 + (text[1]-ca[1])**2
    distB = (text[0]-cb[0])**2 + (text[1]-cb[1])**2
    if distA < distB:
        print( "--> closer to first author" )
    elif distA > distB:
        print( "--> closer to second author" )
    else:
        print( "--> equal distance to both authors" )
    
        
filesA = [] # list of filenames for authors
filesB = []
filesU = []
groupA = [] # list of frequencies for authors
groupB = []
groupU = []

centrA = []
centrB = []

countMode = "chars"
count = { "chars" : countChars,
          "words" : countWords }

#process command line arguments without error checking
mode = ""   # how to interprete next argument
out = 0     # what to print
for arg in sys.argv:
    if arg == "-A" or arg == "-B" or arg == "-L" or arg == "-U":
        mode = arg
    elif arg == "-S":
        out = 1
    elif arg == "-wc":
        countMode = "words"
    else:
        if mode == "-A": # following strings are considered filenames for first author
            filesA.append( arg )
        elif mode == "-B": # following strings a considered filenames for second author
            filesB.append( arg )
        elif mode == "-L": # next strign is the alphabet to use
            strAlph = arg.lower()
        elif mode == "-U": #following strings are filenames of unknown authors
            filesU.append( arg )


        
# minimalistig input check
if len( filesA ) < 1:
    print( "usage: count.py -A <texts by author 1> [-B <texts by author b>] [-L <alphabet>]" )
    print( "optional: -wc\tanalyse word- and sentence lengths" )
    print( "          -S\tplot statistics of frequencies instead of correlation" )
    print( "          -U <..>\tfiles to match with authors" )
    exit( 1 )

# read frequencies from files ( not during command line processing
# because alphabet could change inbetween )
else:
    for aFile in filesA:
        print( "First author: scanning " + aFile )
        groupA.append( count[countMode]( aFile ) )
    if len( groupA ) > 0:
        centrA = calcCentroid( groupA )
    
    for aFile in filesB:
        print( "Second author: scanning " + aFile )
        groupB.append( count[countMode]( aFile ) )
    if len( groupB ) > 0:
        centrB = calcCentroid( groupB )

    for aFile in filesU:
        if len( groupB ) < 1 or len( groupA ) < 1:
            print( "Need to know two authors to identify texts" )
            exit( 2 )
        print( "Unknown: scanning " + aFile )
        groupU.append( count[countMode]( aFile ) )
        authorMatch( groupU[len(groupU)-1], centrA, centrB )

    if out == 1 or len( groupB ) < 1: # plot graph of individual frequencies
        for element in groupA:
            pplot.plot( element, 'b+' )
        for element in groupB:
            pplot.plot( element, 'g+' )
        for element in groupU:
            pplot.plot( element, 'r+' )
        pplot.show()

    elif out == 0: # scatterplot
        centrA = calcCentroid( groupA )
        centrB = calcCentroid( groupB )
        ax = []; bx =[]; cx = []
        ay = []; by =[]; cy = []
        for element in groupA:
            pplot.plot( element[0], element[1], 'bx' )
        for element in groupB:
            pplot.plot( element[0], element[1], 'gx' )
        for element in groupU:
            pplot.plot( element[0], element[1], 'rx' )
        pplot.plot( centrA[0], centrA[1], 'bo' )
        pplot.plot( centrB[0], centrB[1], 'go' )
        pplot.show()
