# Reads data from a FASTA file, creates hashed prefix- and suffix databases. Provides functions to
# find suffixes/prefixes of a given length and to assemble contigs using a greedy algorithm.
# BioPython is not installed on my main system, so some BioPython functions are emulated.

import re
import random

suffixes = {}      # dictionary of suffixes
prefixes = {}      # dictionary of prefixes
ntmap = "ACTG"     # map of nucleotides, needed for hashing
MAX_ELONGATION = 3 # number of nucleotides that may be skipped when elongating greedyly


# reads data from FASTA text file by ignoring non-data lines and stores it into prefix- and suffix databases
# workaround for missing BioPython
def getData( fileName ):   
    sequences = set()
    sequenceParser = re.compile( "[ACTG]{1,}" )# filter problematic newlines
    inFile = open( fileName, "r" )
    print( "Reading file " + fileName + "..." )
    data = False
    for line in inFile:
        if data:                         # skip non-data lines
            sequences.add( sequenceParser.findall( line )[0] )
        data = not data                  # switch data/non-data mode after every line
    inFile.close()

    # add reverse complements
    sc = sequences.copy()
    for seq in sc:
        sequences.add( revComplement( seq ) )

    # hash all elements
    print( "Creating hash maps..." )
    for seq in sequences:
        addSequence( seq, suffixes )       # hash sequence in suffix database
        addSequence( seq[::-1], prefixes ) # hash inverted sequence in prefix database

        

# finds the complement to a given samplestring and returns it backwards
# once more because no BioPython here
# input: a    String sequence of ACTG, other chars would lead to strange output strings
# outpot      reversed complementary string
def revComplement( a ):
    a = a.replace( "A", "t" ).replace( "G", "c" ).replace( "T", "a" ).replace( "C", "g" ).upper()
    return a[::-1]


# calculates a base-4 hash value for a sequence
# input: seq   String sequence of ACTG, other chars would lead to malformed hash values
# output       Integer hash value
def getHash( seq ):
    h = 0                               # hash value
    b = 1                               # base of addition
    for i in range( 9, -1, -1 ):    #...although direction wouldn't matter
        h += ntmap.find( seq[i] ) * b   # add 0-3 * base
        b *= 4                          # increase base
    return h


# stores a key-value pair in the given dictionary
# input: seq       string sequence
#        database  dictionary in which to store (optional, default is suffix-database)
def addSequence( seq, database=suffixes ):
    key = getHash( seq )
    if not key in database:
        database[key] = []
    database[key].append( seq )
    

# finds a suffix of given length
#  seq   sequence that requires a suffix
#  n     length of desired suffix
#  db    database to search( suffix or prefix )
def findSuffix( seq, n, db=suffixes ):
    key = getHash( seq[n:] )          # hash substring of seq shortened by n
    val = db.pop( key, None )
    if val == None:
        return None
    else:
        found = ""
        for suf in val:
            if suf.startswith( seq[n:] ): # compare prefixes
                if found != "":       # ambigous suffix, cancelling
                    return None
                else:
                    found = suf
        if db == suffixes:            # remove used reads from prefix database
            removePrefix( suf )
        return found[-n:]             # return suffix


# removes a prefix from the database, to be called after it has been used as a suffix
# input: p    prefix as string
def removePrefix( p ):
    p = p[::-1]
    key = getHash( p )
    val = prefixes.pop( key, None )
    if val != None and len( val ) > 1:
        prefixes[key] = []
        for el in val[1:]:
            prefixes[key].append( el )
    pass


# elongate a given contig to the right
# input: contig    string to be elongated
#        l         length of reading frame
#        database  database to use (prefixes or suffixes)
# returns: contig as string
def elongate( contig, l, database ):
    found = True
    while found:                        # loop: add suffixes until ambiguities are encountered
        for j in range( 0, MAX_ELONGATION ): # try elongating by 1..MAX_ELONGATION nts
            ext = findSuffix( contig[-l:], j+1, database )
            if ext != None:
                break
        if ext != None:
            contig += ext
        else:
            found = False               # if no disambigous match could be found, end elongating
    return contig
    
            

# creates a contig from a random seed
# extends the contig forward in steps from 1 to max_extend until no elongation possible
# then reverses the contig and repeats the same procedure with reversed suffixes = prefixes
# returns the contig as string
def createContig():
    contig_seed = suffixes.pop( random.choice( list( suffixes.keys() ) ) ) # start with random sequence
    print( "Starting contig_seed from: ", contig_seed[0] )
    if len( contig_seed ) > 1:       # check if more than one element was in the popped hash
        for seq in contig_seed[1:]:  # push back unused sequences
            addSequence( seq )
    contig = contig_seed[0]          # extract first sequence from the random hash
    l = len( contig )                # set length of read frame
    contig = elongate( contig, l, suffixes )  # elongate to the right 
    contig = elongate( contig[::-1], l, prefixes ) # elongate to the left by elongating inverse to the right
    print( "Contig of length ", len( contig ), " could be created" )
    return contig[::-1]                 # invert contig back and return it



# MAIN

getData( "smallReads.fna" )
contigs = []
random.seed()
print( createContig() )

