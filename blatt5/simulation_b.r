# mean coverage of N reads of length 30 with nTest samples of N from 1:1000

l <- 1000         # length of "genome"
R <- 30           # read length
nTests = 500      # how often should new reads be produced
statistics <- vector( mode="numeric", length=l )

for ( n in 1:nTests ) {
  N <- sample( c(10:l), 1 )    # define random read number
  reads <- sample( c(1:l), N ) # produce new reads
  overlap <- vector( mode="logical", length=l ) # reset overlaps
  for ( i in 1:N ) {          
    overlap[reads[i]:(reads[i]+R)] <- TRUE
  }
  # find average coverage
  if ( statistics[N] == 0.0 ) statistics[N] <- mean( overlap )
  else statistics[N]<-mean( c(statistics[N], mean( overlap ) ) )
}
X11()             # initialise window on system with X11 server
barplot( statistics, names.arg=c(1:l) )
dev.copy( pdf, "meanNreadsR30.pdf" )
dev.off()
