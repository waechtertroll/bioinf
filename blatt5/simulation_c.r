l <- 1000         # length of "genome"
R <- 30           # read length
nTests = 100      # how often should new reads be produced
statistics <- vector( mode="numeric", length=l )
readcounts <- vector( mode="numeric", length=l )

for ( n in 1:nTests ) {
  N <- sample( c(10:l), 1 )    # define random read number
  readcounts[N] <- readcounts[N] + 1.0 # shorthand?
  reads <- sample( c(1:l), N ) # produce new reads
  print(  N )                  # ping, I'm still working fine...
  overlap <- vector( mode="logical", length=l ) # reset overlaps
  for ( i in 1:N ) {          
    for ( j in 0:(R-1) ) {
      k <- reads[i]+j
      if ( k <= 1000 ) overlap[k] <- TRUE
    }
  }
  # find average coverage
  if ( mean( overlap ) >= .99 ) statistics[N] <- statistics[N]+1.0
}
statistics = statistics / readcounts # calculate propability
X11()
barplot( statistics, names.arg=c(1:l) )
dev.copy( pdf, "simulation_c.pdf" )
dev.off()

