#!/usr/bin/python
# Python3
# Reads a FASTA-file and creates simulated reads from it
# call with -h for options

import argparse
import random as rnd
import re
import sys

# read FASTA file without BioPython. Assuming only one genome in file.
def readFile( fileName ):
    genome = ""
    sequenceParser = re.compile( "[ACTG]{1,}" )
    inFile = open( fileName, "r" )
    for line in inFile: genome += sequenceParser.findall( line )[0]
    inFile.close()
    return genome

# create fragments
def createFrags( fileName, l, p, n ):
    outFile = open( fileName, "w" ) if fileName != "" else sys.stdout
    global genome
    errors = 0
    nts = ['A','G','C','T']
    for i in range( 0, n ):
        pos = rnd.randint( 0, len( genome )-l )
        fragment = genome[pos:pos+l]
        outFile.write( "> read "+str(i+1)+"\n" )
        if p == 0.0: outFile.write( fragment+"\n" )
        else:
            erroneous = ""
            for char in fragment:
                if ( rnd.random() <= p ): erroneous += rnd.choice( nts )
                else: erroneous += char
            if erroneous != fragment: errors += 1
            outFile.write( erroneous+"\n" )
    print( errors, " reads are erroneous" )

# MAIN

#process input
parser = argparse.ArgumentParser()
parser.add_argument( "-i", help="FASTA input file", dest="inFileName", default="NC_001669.fna" )
parser.add_argument( "-o", help="output file", dest="outFileName", default="" )
parser.add_argument( "-l", help="read length", type=int, dest="fragLen", required=True )
parser.add_argument( "-e", help="error rate", type=float, dest="error", default=0.0 )
parser.add_argument( "-n", help="number of reads to create", type=int, dest="fragNum", default=40000 )
args = parser.parse_args()

genome = readFile( args.inFileName )
createFrags( args.outFileName, args.fragLen, args.error, args.fragNum )
