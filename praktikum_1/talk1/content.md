# Introduction

## Introduction

### What is a microbiome?

-   general
    -   entirity of microorganisms normally living on a creature
    -   distinct biomes on surface \& inside of skin, salive and mucosa
    -   includes bacteria, fungi, archaea
    -   about 10x as many as cells on the human
-   functions
    -   digestive enzyme activity
    -   synthesis of vitamins
    -   interaction with immune system
    -   protection from pathogens

<!--
### The Human Microbiome Project

-   "logical extension of the Human Genome Project"
-   project of the US National Institute of Health
-   goals:
    -   identify microorganisms inside healthy/diseased humans, concentrating on 
    oral, skin, vaginal, gut, nasal/lung microbiomes
    -   long term: analyse health effects of changes in microbiomes
-   methods:
    -   metagenomic analysis of single communities
    -   whole genome sequencing of certain parts of the community
-->

### What to learn from microbiomes

<!-- \begin{columns} -->
<!-- \column{0.5\textwidth} -->

-   taxonomic diversity
    -   number and composition of microbial communities
    -   Operational Taxonomic Units (OTUs)
    -   population diversity

<!-- \column{0.5\textwidth} -->

-   functional metagenomics
    -   functions of communities
    -   proteins
    -   enzymes

<!-- \end{columns} -->

## Human Gut Microbiome Viewed Across Age and Geography

### Human Gut Microbiome Viewed Across Age and Geography

-   part of the Human Microbiome Project

| Origin    | individuals | families | age 0-17 | age 18-70 |
|-----------|------------:|---------:|---------:|----------:|
| Malawi    |         115 |       34 |       83 |        31 |
| Venezuela |         100 |       19 |       65 |        35 |
| USA       |         316 |       98 |      178 |       136 |
|           |             |          |          |           |
| total     |         531 |      151 |      326 |       202 |

## Impact
### Impact

-   demonstration project to search for patterns related to locations and 
    lifestyles
-   understand how westernization changes microbiomes
-   understand nutritional needs
-   increase sustainability of agriculture
  
# Methods applied

## Sampling

### Methods: 16s rRNA

<!-- \begin{center}\includegraphics[width=0.8\textwidth]{16srrna.jpg}\end{center} -->

-   component of the 30S small subunit of prokaryotic ribosomes
-   constant (C$_n$) regions $\to$ location
-   "hypervariable" (V$_n$) regions: $\to$ identify
    -   moleculare clock $\to$ phylogeny
    -   analyze taxonomy

### Methods: Whole genome sequencing

-   shotgun sequencing \& reassembly
-   subset of samples used
-   functional analysis

## Analysis
### Analysis of 16s rRNA

-   UniFrac distance
    -   shared phylogeny branch fraction
    -   $\to$ phylogenetic distance
-   clustered into OTUs (by genetic similarity)
    -   Greengenes database
    -   OTU measure: 97% similarity
-   diversities of OTUs compared

### Analysis of whole genome sequences

-   BLAST against KEGG and COG databases
    -   mapped with 95% similarity
-   Random Forest analysis (OTU abundances $\to$ US/non US)

# Results \& conclusions

## Taxonomic composition
### Evolution of phylogenetic composition

<!-- \centering -->
<!-- \includegraphics[width=0.8\textwidth]{1a} -->

$\Rightarrow$ Child- to adult-composition within first three years

### Evolution of phylogenetic composition

<!-- \centering -->
<!-- \includegraphics[width=0.9\textwidth]{s3} -->

- *Bifidobacterium longum*: sugar $\to$ lactic acid, acetic adic
- Ruminococcaceae: buthyrate productions

### Comparison of compositions

<!-- \begin{center}\includegraphics[width=0.6\textwidth]{1b}\end{center} -->

-   interpersonal variation greater among children
-   significant differences between countries (esp. USA)
-   no significance between villages/regions inside countries

### Comparison of compositions

<!-- \centering -->
<!-- \includegraphics[width=0.8\textwidth]{2a} -->

$\Rightarrow$ diversity increases with age

## Predictive clustering
### Clustering analysis

-   Random Forest analysis
    -   younger: *Bifidobacterium longum*
    -   strong predictors for US $\leftrightarrow$ non-US
    -   weaker predictors Malawia $\leftrightarrow$ Venezuela

<!--
    -   no discrete clusters of Bifidobacteria, Prevotella, Bacteroides
        reproduced
    -   instead Prevotella/Bacteroides tradeoff
-->

## Functional cluster analysis
### Shared functional changes over time

-   no unicque ECs (Enzyme Commission id)
-   total num of ECs constant
-   assignable ECs decline with age - increasing complexity?
-   differences in vitamine synthesis, fermentation pathways

### Shared functional changes over time

| young          | old                |
|:--------------:|:------------------:|
| folate de novo | dietary folate     |
| lactic acid    | methanogenesis     |
| cis            | arg, glu, asp, lys |

$\Rightarrow$ confirms other researchers' findings

### Population- and age-specific differences

| US                  | $\Delta t$ | non-US           | $\Delta t$ |
|---------------------|----------|------------------|----------|
|                     |          | B2 biosynthesis  | -        |
| $\alpha$-fucosidase |   +      | specific glycans | -        |
| urease              | low      | urease           | -        |

-   urease: nitrogen recycling
-   $\alpha$-fucosidase, glycans: oligosaccharide metabolism

### Population- and age-specific differences

<!-- \centering\includegraphics[width=\textwidth]{3} -->

### Differences in adult fecal microbiomes

-   differential ECs: 
    -   US $\leftrightarrow$ non-US: 893
    -   Malawian $\leftrightarrow$ Amerindian 445
-   non-US:
    -   glu-synthase
    -   starch degradation
-   US:
    -   degradation of asp, pro, lys, gln, ornithin, simple sugars, sugar-      substitutes, host glycans
    -   biosynthesis of vitamins, biotin, lipoic acid
    -   metabolism of xenobiotics (aromatic compounds, mercury), bile salts

$\Rightarrow$ parallels to difference carnivorous/herbivorous mammals

### Effects of kinship across countries

<!-- \centering\includegraphics[width=0.7\textwidth]{4} -->

-   twin studies *(almost exclusively US twins)*
    -   microbiome heritability is low
    -   monozygotic twins no different from dizygotic twins

### Effects of kinship across countries

-   mother's microbiomes not more similar to child's than father's
-   co-habitating non-biological fathers more similar than other
    families
-   similarities among families consistent across populations

$\Rightarrow$ environmental exposure shapes gut microbiome

### Conclusion

-   nutrition and lifestyle affect microbiome composition
-   inter-personal variances $\gg$ functional variances
-   different needs for ages / cultural backgrounds
    -   health
    -   nutrition

### <!-- \phantom{.} -->

<!-- \centering \huge -->
The end

###

-   16s rRNA image from *www.clinchem.org*
-   additional information: Phillip E. Melton: *Bioinformatic and statistical analysis of microbiome sequence data* In: www.academia.edu