load( "geneExpressionData.rda" )
cat( "dimensions:", dim( geneExpressionData ), "\n" )
cat( "columns: ", head( colnames( geneExpressionData ) ), "\n" )
cat( "rows: ", head( rownames( geneExpressionData ) ), "\n" )

plot.my.image <- function( ged, ttitle="" ) {
  x <- 1:nrow( ged )
  y <- 1:ncol( ged )
  image( x, y, ged, axes=FALSE )                                           # plot without axis labels
  # add y-axis labels with perpendicular row names, without partitions
  axis( 4, seq( 0, nrow(ged), length=nrow(ged)/3 ), labels=rownames(ged)[c(TRUE,FALSE,FALSE)], las=2, line=-0.5, tick=0 )
  # add x-axis labels with perpendicular column names, without partitions
  axis( 1, seq( 0, ncol(ged), length=ncol(ged)/3 ), labels=colnames(ged)[c(TRUE,FALSE,FALSE)], las=2, line=-0.5, tick=0 )
  axis( 2, y, tick=0, labels=y, line=-.5 )
  title( ttitle )                                                    # add title
}

par( mfrow=c(1,2), mar=c(5.1, 4.0, 4.1, 6.0) )                       # prepare for plot output

plot.my.image( geneExpressionData, "unmodified" )
plot.my.image( geneExpressionData - rowMeans( geneExpressionData ), "means substracted" )
# divide by sd; won't change visible results
#plot.my.image( (geneExpressionData - rowMeans( geneExpressionData )) / apply( ged, 2, sd ) )
