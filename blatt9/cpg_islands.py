from mybio import readFASTA
from math import log
import matplotlib.pyplot as plt
import argparse

# define log odds ratios for CPG-islands
CPG_ODDS_RATIOS = [
    log( 0.1 / 0.25 ),  # A
    log( 0.4 / 0.25 ),  # C
    log( 0.4 / 0.25 ),  # G
    log( 0.4 / 0.25 ) ] # T

# calculate log-odds-ratio for a sequence
# seq       string of nucleotides
# ratios    list of sorted precalculated log-odds-ratios
# returns   float log odds ratio for the complete string
def calc_ratio( seq, ratios=CPG_ODDS_RATIOS ):
    odds_ratio = 0.0
    for base in seq:
        odds_ratio += ratios['ACGT'.find( base )]
    return odds_ratio

# calculate log-odds-ratios for sequences in a sliding read window
# seq       string complete sequence
# wnd_size  integer size of the window
# ratios    list of sorted, precalculated log-odds-ratios
# returns   list of log-odds-ratios for every position
def window_ratio( seq, wnd_size, ratios=CPG_ODDS_RATIOS ):
    y = []
    for i in range( 0, len(seq)-wnd_size ):
        y.append( calc_ratio( long_seq[i:i+wnd_size] ) )
    return y

#===============================================================================
#   MAIN    
#===============================================================================

parser = argparse.ArgumentParser()
parser.add_argument( '-s', help='sliding window size', default=11, type=int, dest='wnd_size' )
args = parser.parse_args()

seqs = readFASTA( 'snippets.fna', statistics=True )
i = 1
for seq in seqs:
    print( 'Sequence', i, ': ratio(CPG) =', calc_ratio( seq ) )
    i += 1
print()

long_seq = readFASTA( 'longSequence.fna', statistics=True )[0]
y = window_ratio( long_seq, args.wnd_size )

plt.plot( y )
plt.title( 'CpG score with window size of '+str(args.wnd_size) ); plt.xlabel( 'position' ); plt.ylabel( 'CpG score' )
plt.show()

# Sequence 1 : ratio(CPG) = 33.1374193134
# Sequence 2 : ratio(CPG) = 12.3430038966
# Sequence 3 : ratio(CPG) = 15.1155926188
# Sequence 4 : ratio(CPG) = 33.1374193134
# Sequence 5 : ratio(CPG) = 33.1374193134
# Sequence 6 : ratio(CPG) = 33.1374193134
# Sequence 7 : ratio(CPG) = 13.7292982577
# Sequence 8 : ratio(CPG) = 17.8881813411
# Sequence 9 : ratio(CPG) = 9.57041517434
# Sequence 10 : ratio(CPG) = 27.5922418689
# Sequence 11 : ratio(CPG) = 31.7511249523
# Sequence 12 : ratio(CPG) = 8.18412081322
# Sequence 13 : ratio(CPG) = 34.5237136745
# Sequence 14 : ratio(CPG) = 12.3430038966
# Sequence 15 : ratio(CPG) = 33.1374193134
# Sequence 16 : ratio(CPG) = 12.3430038966
# Sequence 17 : ratio(CPG) = 34.5237136745
# Sequence 18 : ratio(CPG) = 23.4333587855
# Sequence 19 : ratio(CPG) = 8.18412081322
# Sequence 20 : ratio(CPG) = 8.18412081322
# Sequence 21 : ratio(CPG) = 13.7292982577
# Sequence 22 : ratio(CPG) = 1.25264900762
# Sequence 23 : ratio(CPG) = 31.7511249523
# Sequence 24 : ratio(CPG) = 8.18412081322
# Sequence 25 : ratio(CPG) = 9.57041517434
# Sequence 26 : ratio(CPG) = 24.8196531467
# Sequence 27 : ratio(CPG) = 12.3430038966
# Sequence 28 : ratio(CPG) = 8.18412081322
# Sequence 29 : ratio(CPG) = 31.7511249523
# Sequence 30 : ratio(CPG) = 31.7511249523
# Sequence 31 : ratio(CPG) = 10.9567095355
# Sequence 32 : ratio(CPG) = 31.7511249523
# Sequence 33 : ratio(CPG) = 23.4333587855
# Sequence 34 : ratio(CPG) = 1.25264900762
# Sequence 35 : ratio(CPG) = 28.97853623
# Sequence 36 : ratio(CPG) = 31.7511249523
# Sequence 37 : ratio(CPG) = 33.1374193134
# Sequence 38 : ratio(CPG) = 28.97853623
# Sequence 39 : ratio(CPG) = 12.3430038966
